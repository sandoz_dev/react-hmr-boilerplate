# README #

This repo will give you a react app with a single component. Hot module reloading is already configured using webpack 2 and babel.

You will need to either add a build step to copy the `web/index.html` into a `dist` directory, or you can just manually create one to start and copy the file in.

**Please do not use this build for production. You should create a second webpack config file to build your code for any production release.**