import * as React from 'react';
import styles from './app.css';

const App = () => {
    return (
        <div>
            <h1 className={styles.highlight}>React hot loading like a boss! 1, 2, 3, 4, 5!</h1>
        </div>
    )
}

export default App;